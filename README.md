# ProxyPack: Proxy Magager for Windows

ProxyPack manage settings for various softwares.

## Overview

ProxyPack set proxy for:  
- Environment Variables (HTTP_PROXY, HTTPS_PROXY, FTP_PROXY, NO_PROXY)
- WinHTTP Proxy
- Web Proxy (Internet Explorer)

## Usage

1. Locate config JSON file:  
    %APPDATA%\ProxyPack\config.json

2. (ProjectDir)\shims\proxypack.bat is executable and toggle proxy settings.

## Licence
Copyright (c) 2017 rabe.  
Released under the [MIT Licence](https://opensource.org/licenses/mit-license).
