function Check-If-Admin() {
    $windowsPrincipal = [Security.Principal.WindowsPrincipal] `
        [Security.Principal.WindowsIdentity]::GetCurrent()
    return $windowsPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
}

function Enable-Proxy-Env( $domain, $port, $noProxy) {
    $address = "http://${domain}:${port}"
    # Set envrironment variables
    [System.Environment]::SetEnvironmentVariable("HTTP_PROXY", $address, "User")
    [System.Environment]::SetEnvironmentVariable("HTTPS_PROXY", $address, "User")
    [System.Environment]::SetEnvironmentVariable("FTP_PROXY", $address, "User")
    [System.Environment]::SetEnvironmentVariable("NO_PROXY", $noProxy, "User")
}

function Disable-Proxy-Env() {
    [System.Environment]::SetEnvironmentVariable("HTTP_PROXY", $null, "User")
    [System.Environment]::SetEnvironmentVariable("HTTPS_PROXY", $null, "User")
    [System.Environment]::SetEnvironmentVariable("FTP_PROXY", $null, "User")
    [System.Environment]::SetEnvironmentVariable("NO_PROXY", $null, "User")
}

function Enable-Proxy-Winhttp( $domain, $noProxy ) {
    # <<ADMIN REQUIRED>>
    $proxy = New-Object System.Net.WebProxy $domain
    [System.Net.WebRequest]::DefaultWebProxy = $proxy

    netsh winhttp set proxy proxy-server=$domain bypass-list=$noProxy
}

function Disable-Proxy-Winhttp() {
    # <<ADMIN REQUIRED>>
    [System.Net.WebRequest]::DefaultWebProxy = $null

    netsh winhttp reset proxy
}

function Enable-Proxy-Internet( $domain, $port, $noProxy ) {
    # <<ADMIN REQUIRED>>
    # Get Internet Setting from registory
    $reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    # Set proxy
    Set-ItemProperty -Path $reg -Name ProxyServer "${domain}:${port}"
    Set-ItemProperty -Path $reg -Name ProxyEnable -Value 1
    Set-ItemProperty -Path $reg -Name ProxyOverride "${noProxy};<local>"
}

function Disable-Proxy-Internet() {
    # <<ADMIN REQUIRED>>
    # Get Internet Setting from registory
    $reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
    Remove-ItemProperty -Path $reg -Name ProxyServer
    Set-ItemProperty -Path $reg -Name ProxyEnable -Value 0
    Remove-ItemProperty -Path $reg -Name ProxyOverride

}

#============================== Main ==================================================

# Check if admin user.
$admin = Check-If-Admin
if ( -Not $admin ) {
    Write-Output "Error: Only Admin user allowed to execute this command."
    Exit
}

# All Proxy Settings refecence to Internet Settings

# Check if proxy of Internet Settings is enable.
$reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
$proxyEnable = Get-ItemPropertyValue -Path $reg -Name ProxyEnable

# Toggle proxy settings
if ( $proxyEnable -eq 0 ) {
    # Enable Proxy when Proxy off

    Write-Output "Set proxy on ...."

    # JSON file location
    $jsonFile = "${Env:APPDATA}\ProxyPack\config.json"

    # Load config from JSON
    $config = $(Get-Content $jsonFile -Encoding UTF8 -Raw | ConvertFrom-Json)

    $domain = $config.Address
    $port = $config.Port
    $noProxy = $config.NoProxy

    # Check if Address and Port is set.
    if ( [string]::IsNullOrEmpty( $domain ) `
            -or [string]::IsNullOrEmpty( $port ) `
            -or [string]::IsNullOrEmpty( $noProxy ) ) {
        Write-Output "Error:  Address or Port is not set."
        exit
    }

    Write-Output "      Domain=${domain}"
    Write-Output "      Port=${port}"
    Write-Output "      No Proxy=${noProxy}"

    Enable-Proxy-Env $domain $port $noProxy
    Enable-Proxy-Winhttp $domain $noProxy
    Enable-Proxy-Internet $domain $port $noProxy

    Write-Output "Proxy enabled."
}
else {
    # Disable Proxy when Proxy on

    Write-Output "Set proxy off ...."

    Disable-Proxy-Env
    Disable-Proxy-Winhttp
    Disable-Proxy-Internet

    Write-Output "Proxy disabled."
}

Write-Output "Prease restart shell or execute refreshenv command."
