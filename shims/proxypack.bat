@echo off
:   File:
:       proxypack.bat
:   Description:
:       Set Proxy


: Move to <Project>/bin directory
pushd %~dp0

: Execute Powershell script
call powershell -NoProfile -ExecutionPolicy Unrestricted ..\scripts\proxypack.ps1

: Return to previous directory
popd
